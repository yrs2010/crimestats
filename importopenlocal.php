<?php

require "rss.php";
$feed = new SimplePie();

require "CouchDB.php";
$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984);

$openlylocals = json_decode(file_get_contents("http://openlylocal.com/hyperlocal_sites.json"));

foreach ($openlylocals as $newssite) {
  $site = $newssite->hyperlocal_site;
  $lat = $site->lat;
  $lon = $site->lng;
  
  if (!$site->feed_url) {
    continue; // no feed!
  }
  
  $feed->set_feed_url($site->feed_url);
  $feed->init();
  $feed->handle_content_type();
  
  if ($feed->get_items() && is_array($feed->get_items())) {
  foreach ($feed->get_items() as $item) {
    if (preg_match("/(rape|murder|crime|jail|prison|harass|fraud|theft|robbe|burgl)/", ( $item->get_description()." ".$item->get_title() ) )) {
      $itemarray = array();
      $itemarray['fields']['body'] = $item->get_description();
      $itemarray['fields']['headline'] = $itemarray['webTitle'] = $item->get_title();
      $itemarray['webUrl'] = $item->get_permalink();
      $itemarray['webPublicationDate'] = $item->get_date('r');
      $itemarray['type'] = 'openlocal';
      $itemarray['lat'] = $lat;
      $itemarray['long'] = $lon;

      $couchdb->update($itemarray, $item->get_permalink());
    } }
  }
}
?>