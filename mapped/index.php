<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
  function initialize() {
    var latlng = new google.maps.LatLng(54.901822, -3.120117);
    var myOptions = {
      zoom: 5,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    

    
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
        
<?php
  require '../CouchDB.php';
$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984);

try {
    $result = $couchdb->view("test", "guardian");
} catch(CouchDBException $e) {
    die($e->errorMessage()."\n");
}
$all_docs = $result->getBody(true);

$i = 1;

$windows = "";
$markers = "";
$listeners = "";
foreach ($all_docs->rows as $row) {
    $article = $row->value;
    if ($article->lat->{0} && $article->long->{0}) {
    $windows .= "var infowindow$i = new google.maps.InfoWindow({
    content: '<a href=\"".$article->webUrl."\">".htmlspecialchars($article->webTitle, ENT_QUOTES)."</a>'
    });";
    
 $markers .= "var marker$i = new google.maps.Marker({
    position: new google.maps.LatLng(".$article->lat->{0}.", ".$article->long->{0}."),
    map: map,
});";

  $listeners .= "google.maps.event.addListener(marker$i, 'click', function() {
  infowindow$i.open(map,marker$i);
});";
  
  if ($i == 125) {
    break;
  }
  
  $i += 1; }
}

print $windows.$markers.$listeners;
?>
  
  }

</script>
</head>
<body onload="initialize()">
  <div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>