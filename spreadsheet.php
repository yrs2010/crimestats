<?php

function fuzzy_match($needle, $haystack) {
    $closest = array("lev"=>100);
    foreach ($haystack as $key=>$value) {
        $l = levenshtein($needle, $value);
        if ($l < $closest["lev"]) {
            $closest = array("key"=>$key, "value"=>$value, "lev"=>$l);
        }
    }
    return $closest;
}

$f = fopen("SNAC2009.csv", "r");
$header = fgetcsv($f);
$las = array();
$reverselas = array();
while ($row = fgetcsv($f)) {
    #print_r($row);
    $lacode = substr($row[10],0,4);
    if (!in_array($lacode, $las)) {
        $las[$lacode] = $row[7];
        $reverselas[$row[7]] = $lacode;
    }
}
#print_r($las);
print_r($reverselas);
#exit();

$f = fopen("laa1.csv", "r");
for ($i=0; $i<4; $i++) {
    fgetcsv($f);
}
$header = fgetcsv($f);
print_r($header);

require "CouchDB.php";

$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984);

while ($row = fgetcsv($f)) {
    if ($row[0]) {
        if ($row[0] == "England and Wales") break;
        if ($row[1] != "Total") {
            echo $row[1]."\n";
            $lainfo = fuzzy_match($row[1], $las);
            for ($i=2; $i<sizeof($row); $i++) {
                $row[$i] = str_replace("+","",str_replace(".","",str_replace(",","",$row[$i])));
            }
            $crimeinfo = array(
                "2008" => array("violent"=>$row[4], "sexual"=>$row[8], "robbery"=>$row[12], "burglary"=>$row[16], "theft_of_vehicle"=>$row[21], "theft_from_vehicle"=>$row[25], "interfering_vehicle"=>$row[29], "bcs"=>$row[33]),
                "2009" => array("violent"=>$row[5], "sexual"=>$row[9], "robbery"=>$row[13], "burglary"=>$row[17], "theft_of_vehicle"=>$row[22], "theft_from_vehicle"=>$row[26], "interfering_vehicle"=>$row[30], "bcs"=>$row[34])
            );
            $doc = array(
                "name"=>$row[1],
                "name_matched"=>$lainfo["value"],
                "population"=>$row[2],
                "households"=>$row[3],
                "crimes"=>$crimeinfo,
                "lacode"=>$lainfo["key"],
                "type"=>"lastats"
            );
            if ($d = $couchdb->get_item("stats".$lainfo["key"])) $doc["_rev"] = $d->getBody(true)->_rev;
            $couchdb->update($doc, "stats".$lainfo["key"]);
        }
    }
}

?>