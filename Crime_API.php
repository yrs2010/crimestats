<?php

/*
 * Basic wrapper to make things easier.
 * Wraps http://policeapi.rkh.co.uk/.
 */
class Crime_API
{
	const API_KEY = '3a737a45f66b3b47a33d13d463e55bbc',
		  API_BASE = 'http://policeapi.rkh.co.uk/api/';
	
	public static function getInstance ()
	{
		static $_instance;
		if (!$_instance)
		{
			$className = __CLASS__;
			$_instance = new $className;
		}
		
		return $_instance;
	}
	
	public function __call ($methodName, $methodParams = array())
	{
		$params = (count($methodParams) > 0) ? $methodParams[0] : array();
		$reqURL = $this->_buildURL($methodName, $params);
		$apiResponse = $this->_queryAPI($reqURL);
		
		$parsedData = simplexml_load_string($apiResponse);
		return $parsedData;
	}
	
	protected function _buildQueryString ($params)
	{
		$query = '';
		foreach ($params as $key => $value)
		{
			$query .= $key . '=' . $value . '&';
		}
		trim($query, '&');
		return $query;
	}
	
	protected function _buildURL ($uri, $params = array())
	{
		return self::API_BASE . $uri . '?key=' . self::API_KEY . '&' . $this->_buildQueryString($params);
	}
	
	protected function _queryAPI ($url)
	{
		return file_get_contents($url);
	}
}