<?php
// compare news stories & their locations to actual statistics

require 'CouchDB.php';
$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984);

// Number of news stories per local authority
try {
    $result = $couchdb->view("test", "guardianlas", "group=true");
} catch(CouchDBException $e) {
    die($e->errorMessage()."\n");
}
$all_docs = $result->getBody(true);
$newscount = array();
foreach($all_docs->rows as $row) {
    $newscount[$row->key] = $row->value;
}

// Actual statistics per local authority
try {
    $result = $couchdb->view("test", "lastats");
} catch(CouchDBException $e) {
    die($e->errorMessage()."\n");
}
$all_docs = $result->getBody(true);
echo "<table border=\"1\">";
echo "<tr><td>LA Code</td><td>LA Name</td><td>Crime count</td><td>Crime news count</td><td>Fraction</td></tr>";
foreach($all_docs->rows as $row) {
    $la = $row->value->lacode;
    if (isset($newscount[$la])) {
      echo "<tr>";
      echo "<td>". $la ."</td>";
      echo "<td>". $row->value->name ."</td>";
      echo "<td>". $row->value->crimes->{2009}->bcs ."</td>";
      echo "<td>". $newscount[$la] ."</td>";
      echo "<td>". $row->value->crimes->{2009}->bcs/$newscount[$la] ."</td>";
      echo "</tr>";
    }
}
echo "</table>";

?>