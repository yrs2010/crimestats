<?php
// Add local authority codes to news stories
require 'CouchDB.php';
$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984);

for ($i=0; $i<1000000; $i+=100) {
try {
    $result = $couchdb->view("test", "guardian", "limit=100&skip=".$i);
} catch(CouchDBException $e) {
    die($e->errorMessage()."\n");
}

$all_docs = $result->getBody(true);

foreach($all_docs->rows as $row) {
    $doc = $row->value;
    if (property_exists($doc, "lat") && property_exists($doc, "long")) {
        if (is_string($doc->lat)) $lstring = $doc->lat . "," . $doc->long;
        else {
            print_r($doc->lat);
            $lstring = $doc->lat->{0} . "," . $doc->long->{0};
        }
        echo $lstring."\n";
        if ($lstring != "," and $lstring != "0,0" and !property_exists($doc, "lacode")) {
            $json = json_decode(file_get_contents("http://www.uk-postcodes.com/latlng/".$lstring.".json"));
            $lacode = $json->administrative->district->snac;
            $wardcode = $json->administrative->ward->snac;
            echo $lacode . " " . $wardcode . "\n";
            $doc->lacode = $lacode;
            $doc->wardcode = $wardcode;
            $couchdb->update($doc, $doc->_id);
        }
    }
}
if ($all_docs->total_rows+100 < $i) break;
}

?>