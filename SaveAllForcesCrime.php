<?php

include_once 'CouchDB.php';
include_once 'Crime_API.php';
include_once 'Crime_API_ForceList.php';

$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984);

print_r($couchdb->get_all_docs());
exit;
$Crime_API_ForceList = new Crime_API_ForceList();
$forceIDs = $Crime_API_ForceList->getForceIDs();

$forceResponses = array();
foreach ($forceIDs as $forceID)
{
	$forceAreasLookup = Crime_API::getInstance()->{"crime-areas"}(array('force' => $forceID));
	$forceResponses[$forceID] = Crime_API::getInstance()->{"crime-area"}(array('force' => $forceID, 'area' => $forceAreasLookup->response->area[0]->id));
	foreach ($forceResponses[$forceID]->response->{"total-crimes"}->total as $total)
	{
		$forceCrimeTotals = array(
			'type' => '46-totalCrime',
			'force' => (string) $forceID,
			'area' => (string) $forceAreasLookup->response->area[0]->id,
			'date' => (string) $total->date,
			'total' => (string) $total->value
		);
		$couchdb->update($forceCrimeTotals, $forceID . '_' . $forceAreasLookup->response->area[0]->id . '_' . $forceCrimeTotals['date']);
	}
}