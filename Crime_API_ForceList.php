<?php

include_once 'Crime_API.php';

/*
 * $crime = new Crime_API_FromCoords('52.8836', '-1.97406');
 * print_r($crime->getCrime());
 */

class Crime_API_ForceList
{
	public function getForces ()
	{
		return Crime_API::getInstance()->{"forces"}();
	}
	
	public function getForceIDs ()
	{
		$forces = $this->getForces()->response->force;
		
		$forceIDs = array();
		foreach($forces as $force)
		{
			$forceIDs[] = (string) $force->id;
		}
		
		return $forceIDs;
	}
}