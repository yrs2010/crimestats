<?php
$placekey = 'zwLPHI7V34E8RVohuarV043C77EiCS.Fd9Ji5fDzPpd8xX032DdgvLAhLpnswAhwww--';
$placeapiendpoint = 'http://wherein.yahooapis.com/v1/document';
$placeurl = 'http://uk.yahoo.com';
$placeinputType = 'text/html';
$placeoutputType = 'xml';
$ch = curl_init($placeapiendpoint);

require "CouchDB.php";

$couchdb = new CouchDB("crimestats", "xn--gce.com", 5984); // See if we can make a connection



$currpage = 930;
$maxpages = 1611;

while ($currpage <= $maxpages) {
  print "Starting page $currpage...\n";
  $guardiannews = json_decode(file_get_contents("http://content.guardianapis.com/uk/ukcrime?format=json&show-fields=all&api-key=ztbaaexu9pat3dygy5zaqeys&page=$currpage"));
  foreach ($guardiannews->response->results as $article) {
    print "  Handling id: ". $article->id."...\n";
    
    print "    Trying to find location for ".$article->id."...\n";
    
    $post = 'appid='.$placekey.'&documentURL='.$placeurl.
                  '&documentType='.$placeinputType.
  				'&outputType='.$placeoutputType.
  				'&documentContent='. rawurlencode($article->fields->body);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
    $locdata = curl_exec($ch);
    $locdata = simplexml_load_string($locdata);
    
    $lat = (string) $locdata->document->geographicScope->centroid->latitude;
    $lon = (string) $locdata->document->geographicScope->centroid->longitude;
  
    print "    Found location! (".$lat.", ".$lon.")\n";
    print "  Submitting to CouchDB...\n";
    
    $article->type = 'guardian';
    $article->lat = $lat;
    $article->long = $lon;
    
    if ($d = $couchdb->get_item($article->id)->getBody(true)) {
      if (!isset($d->error))
        $article->_rev = $d->_rev;
    }
    
    $couchdb->update($article, $article->id);
    print "  Submitted.\n";
  }
  $currpage += 1;
}

print "Done!";

?>