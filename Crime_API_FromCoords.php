<?php

include_once 'Crime_API.php';

/*
 * $crime = new Crime_API_FromCoords('52.8836', '-1.97406');
 * print_r($crime->getCrime());
 */

class Crime_API_FromCoords
{
	protected $_longditude, // Coordinates
			  $_latitude,
			  $_force,
			  $_area;
	
	public function __construct ($longditude, $latitude)
	{
		$this->_longditude = $longditude;
		$this->_latitude = $latitude;
		
		$coordsCat = $this->_longditude . ',' . $this->_latitude;
		$coordsLookup = Crime_API::getInstance()->{"geocode-crime-area"}(array('q' => $coordsCat));
		
		$this->_force = (string) $coordsLookup->response->areas->area[0]->{"force-id"};
		$this->_area = (string) $coordsLookup->response->areas->area[0]->{"area-id"};
	}
	
	public function getCrime ($depth = 0)
	{
		$forceAreasLookup = Crime_API::getInstance()->{"crime-areas"}(array('force' => $this->_force));
		
		if ($depth == 0)
		{
			$apiResponse = Crime_API::getInstance()->{"crime-area"}(array('force' => $this->_force, 'area' => $forceAreasLookup->response->area[0]->id));
			return $apiResponse;
		}
		
		//@TODO Add recursing down area tree to requested depth.
		throw new Exception(__CLASS__ . '::' . __FUNCTION__ . ' - Not yet implemented for when Depth > 0.');
	}
}