<?php

class CrimeFromGeocode
{
        public function __construct ($long, $lat)
        {
                $regionInfo = $this->_findRegion($long, $lat);
                return $this->_getRegion($regionInfo['force'], $regionInfo['code']);
        }

        protected function _findRegion ($long, $lat)
        {
                $xml = file_get_contents('http://policeapi.rkh.co.uk/api/geocode-crime-area?key=3a737a45f66b3b47a33d13d463e55bbc&q=' . $long . ',' . $lat);
                $parsed = simplexml_load_string($xml);
                
                return array('force'	=> $parsed->response->areas->area[0]->{"force-id"},
                             'code'		=> $parsed->response->areas->area[0]->{"area-id"});
        }
}

$CrimeFromGeocode = new CrimeFromGeocode('52.8836', '-1.97406');

